package com.kgenov.hibernate.demo.CRUD;
/* konstantin created on 3/5/2021 inside the package - com.kgenov.com.kgenov.hibernate.demo */

import com.kgenov.hibernate.demo.CRUD.Entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteStudentDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure() //filename doesnt need to be specified if you are using the default name
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session

        Session session = factory.getCurrentSession();

        try {
            int studentId = 1; // use for deletion example

            // get a session and open a transaction
            session = factory.getCurrentSession();
            session.beginTransaction();

            // retrieve student by id
            System.out.println("Retrieving student..");
            Student studentForDeletion = session.get(Student.class, studentId);
            System.out.println("Student retrieved: " + studentForDeletion);

            // delete the student
            System.out.println("Deleting student..");
            session.delete(studentForDeletion);

            // delete student where id = 2
            System.out.println("Deleting student where id = 2");
            session.createQuery("delete from Student where id=2").executeUpdate();

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Transaction successful.");

        } finally {
            factory.close();
            System.out.println("Database connection closed.");
        }

    }
}