package com.kgenov.hibernate.demo.CRUD;
/* konstantin created on 3/5/2021 inside the package - com.kgenov.com.kgenov.hibernate.demo */

import com.kgenov.hibernate.demo.CRUD.Entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class ReadStudentDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure() //filename doesnt need to be specified if you are using the default name
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session

        Session session = factory.getCurrentSession();

        try {
            // use session to save the Java object

            // create a new student obj
            System.out.println("Creating a new student object...");
            Student student = new Student("John", "Wick", "jwick@continental.com");

            // start a transaction
            System.out.println("Beginning database transaction..");
            session.beginTransaction();

            // save the student obj
            System.out.println("Saving the student...");
            session.save(student);

            // commit transaction
            session.getTransaction().commit();

            // find out student's id - PK
            System.out.println("Saved student. Generated id: " + student.getId());

            // get a new session and start transaction
            session = factory.getCurrentSession();
            session.beginTransaction();

            // retrieve student based on PK
            System.out.println("Fetching student with id: " + student.getId());

            Student retrievedStudent = session.get(Student.class, student.getId());

            System.out.println("Fetch complete: " + retrievedStudent);

            // query all students
            List<Student> allStudents = session.createQuery("from Student").getResultList();

            // list all students
            displayStudents(allStudents);

            // query students where lastName = Wick
            allStudents = session.createQuery("from Student s where s.lastName='Wick'").getResultList();

            System.out.println("\nStudents who have last name of Wick");
            displayStudents(allStudents);

            // query students: lastName='Wick' or firstName='Boiko'
            allStudents = session.createQuery("from Student s where s.lastName='Wick' OR s.firstName='Boiko'").getResultList();
            System.out.println("\nStudents with first name Boiko and last name Wick");
            displayStudents(allStudents);

            // query students where email LIKE '%abv.bg'
            allStudents = session.createQuery("from Student s where s.email LIKE '%abv.bg'").getResultList();
            System.out.println("\nStudents who have an email ending in abv.bg");
            displayStudents(allStudents);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done! :))");

        } finally {
            factory.close();
            System.out.println("Database connection closed.");
        }

    }

    private static void displayStudents(List<Student> allStudents) {
        for (Student matchedStudents : allStudents
        ) {
            System.out.println(matchedStudents);
        }
    }
}
