package com.kgenov.hibernate.demo.CRUD;
/* konstantin created on 3/5/2021 inside the package - com.kgenov.com.kgenov.hibernate.demo */

import com.kgenov.hibernate.demo.CRUD.Entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class UpdateStudentDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure() //filename doesnt need to be specified if you are using the default name
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session

        Session session = factory.getCurrentSession();

        try {
            int studentId = 1;

            // new session and begin transaction
            session = factory.getCurrentSession();
            session.beginTransaction();

            // retrieve student based on id
            Student retrievedStudent = session.get(Student.class, studentId);

            System.out.println("Student retrieved: " + retrievedStudent);

            // now we are going to change some properties on the student
            System.out.println("Updating student...");
            retrievedStudent.setFirstName("Viki");
            retrievedStudent.setLastName("Slavova");
            retrievedStudent.setEmail("victoria@devs.bg");

            // commit transaction
            session.getTransaction().commit();
            System.out.println("Transaction successful.");

            // new session to perform more updates
            session = factory.getCurrentSession();
            session.beginTransaction();

            // update all users with lastName='Wick'
            System.out.println("Updating email for all students with last name Wick..");

            session.createQuery("update Student set email='foo@bar.com' where last_name='Wick'").executeUpdate();

            // check if students got updated
            List<Student> allStudents = session.createQuery("from Student").getResultList();

            allStudents.forEach(System.out::println);

            // commit transaction
            session.getTransaction().commit();
            System.out.println("Transaction successful.");

        }
        finally {
            factory.close();
            System.out.println("Database connection closed.");
        }

    }
}
