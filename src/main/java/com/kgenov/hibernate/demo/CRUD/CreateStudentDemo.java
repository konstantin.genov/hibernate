package com.kgenov.hibernate.demo.CRUD;
/* konstantin created on 3/5/2021 inside the package - com.kgenov.com.kgenov.hibernate.demo */

import com.kgenov.hibernate.demo.CRUD.Entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateStudentDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure() //filename doesnt need to be specified if you are using the default name
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session

        Session session = factory.getCurrentSession();

        try {
            // use session to save the Java object

            // create a new student obj
            System.out.println("Creating a new student object...");
            Student student = new Student("Boiko", "Borisov", "boiko@abv.bg");

            // start a transaction
            System.out.println("Beginning database transaction..");
            session.beginTransaction();

            // save the student obj
            System.out.println("Saving the student...");
            session.save(student);

            // commit transaction
            session.getTransaction().commit();
            System.out.println("Done! : )");

        }
        finally {
            factory.close();
            System.out.println("Database connection closed.");
        }

    }
}
