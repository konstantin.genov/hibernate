package com.kgenov.hibernate.demo.CRUD.Entity;
/* konstantin created on 3/4/2021 inside the package - com.kgenov.hibernate.demo.CRUD.Entity */

import javax.persistence.*;

/*
 When we map our classes to a database we have to always follow these 2 steps in order:

 Step 1: Map our Java class to the corresponding table name in the DB:
 - 1.1 We need to add the @Entity annotation on our class - this annotation defines that a class can be mapped to a table. And that is it, it is just a marker!
 - 1.2 We need to add the @Table annotation to our class - this allows us to specify the details of the table that will be used to persist the entity in the database.

 Step 2: Map our class fields to the corresponding column names in the DB:
 - 2.1 We need to always add the @Id annotation on our field corresponding to the id in the DB - the annotation indicates the member field below is the primary key of current entity
 - 2.2 We need to add the @Column annotation for each field in our class - this annotation is used to specify the mapped column for a persistent property or field.
 If no Column annotation is specified, the default value will be applied.
 */

@Entity
@Table(name = "student")
public class Student {

    @Id // map to primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id") // map to column, in the name property we ALWAYS need to use the exact name of the column in the database; our field can be named differently, but the column needs to be accurate
    private int id;

    @Column(name = "first_name") // here the column name in the DB differs from our field
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    public Student() {
    }

    public Student(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
