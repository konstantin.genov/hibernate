package com.kgenov.hibernate.demo.FetchTypes.Eager.Entities;
/* konstantin created on 3/12/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToMany.Entities */

import com.kgenov.hibernate.demo.FetchTypes.Eager.Entities.CourseInstructor;

import javax.persistence.*;

@Entity
@Table(name = "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "instructor_id")
    // FK in the course table is instructor_id, which points to id field in instructor table
    private com.kgenov.hibernate.demo.FetchTypes.Eager.Entities.CourseInstructor instructor;


    public Course() {
    }

    public Course(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public com.kgenov.hibernate.demo.FetchTypes.Eager.Entities.CourseInstructor getInstructor() {
        return instructor;
    }

    public void setInstructor(CourseInstructor instructor) {
        this.instructor = instructor;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
