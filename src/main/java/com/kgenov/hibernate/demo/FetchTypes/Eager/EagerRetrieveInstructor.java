package com.kgenov.hibernate.demo.FetchTypes.Eager;
/* konstantin created on 3/14/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToMany */

import com.kgenov.hibernate.demo.FetchTypes.Eager.Entities.Course;
import com.kgenov.hibernate.demo.FetchTypes.Eager.Entities.CourseInstructor;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EagerRetrieveInstructor {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg-one-to-many.xml") // specify correct hibernate file!
                .addAnnotatedClass(CourseInstructor.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();


        Session session = factory.getCurrentSession();

        try {
            session.beginTransaction();

            int instructorId = 2;
            // fetch instructor by id
            CourseInstructor vicky = session.get(CourseInstructor.class, instructorId);

            // print out the instructor
            System.out.println("Instructor: " + vicky);

            // fetch and print all courses taught by the instructor
            System.out.println("\nCourses by the instructor: " + "\n" + vicky.getCourses());

            // commit changes
            session.getTransaction().commit();
            System.out.println("Transaction committed successfully.");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
            System.out.println("Database connection closed.");
        }
    }
}

