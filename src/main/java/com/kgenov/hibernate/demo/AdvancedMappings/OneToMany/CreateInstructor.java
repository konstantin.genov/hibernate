package com.kgenov.hibernate.demo.AdvancedMappings.OneToMany;
/* konstantin created on 3/13/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToMany */

import com.kgenov.hibernate.demo.AdvancedMappings.OneToMany.Entities.Course;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToMany.Entities.CourseInstructor;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateInstructor {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg-one-to-many.xml") // specify correct hibernate file!
                .addAnnotatedClass(CourseInstructor.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();



        Session session = factory.getCurrentSession();

        try {
            session.beginTransaction();

            // Create the course instructor and corresponding instructor detail objects
            CourseInstructor instructor = new CourseInstructor("Viktoria", "Slavova", "vicky@drum-master.com");
            InstructorDetail instructorDetail = new InstructorDetail("https://youtube.com/vicslavova", "playing the drums");

            // create relationship
            instructor.setInstructorDetail(instructorDetail);

            // save the new objects - this also saves the instructor detail due to our cascade type being specified as ALL
            session.save(instructor);

            // commit changes
            session.getTransaction().commit();
            System.out.println("Transaction committed successfully.");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
            System.out.println("Database connection closed.");
        }
    }
}
