package com.kgenov.hibernate.demo.AdvancedMappings.OneToMany.Entities;
/* konstantin created on 3/12/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToMany.Entities */

import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.InstructorDetail;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "instructor")
public class CourseInstructor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    // our relationship is one to one -> One instruct will have one instructor detailed and vice versa
    @OneToOne(cascade = CascadeType.ALL)
    // specify the relationship; we want all operations performed on the instructor to be passed to the instructordetail class as well
    @JoinColumn(name = "instructor_detail_id")
    // we join the tables using the instructor_detail_id as a FK; this is the PK in the instructor_detail table
    private InstructorDetail instructorDetail;

    @OneToMany(mappedBy = "instructor", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private List<Course> courses;

    public CourseInstructor() {
    }

    public CourseInstructor(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public InstructorDetail getInstructorDetail() {
        return instructorDetail;
    }

    public void setInstructorDetail(InstructorDetail instructorDetail) {
        this.instructorDetail = instructorDetail;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    // add methods for bi-directional relationship
    public void addCourse(Course course) {
        if (courses == null) {
            courses = new ArrayList<>();
        }
        courses.add(course);

        course.setInstructor(this); // pass in ref. to current object
    }

    @Override
    public String toString() {
        return "CourseInstructor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", instructorDetail=" + instructorDetail +
                '}';
    }
}
