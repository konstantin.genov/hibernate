package com.kgenov.hibernate.demo.AdvancedMappings.OneToOne;
/* konstantin created on 3/6/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToOne */

import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.BiDirectional.Instructor_BD;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.Instructor;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.InstructorDetail;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.BiDirectional.InstructorDetail_BD;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg-one-to-one-uni.xml") // specify correct hibernate file!
                .addAnnotatedClass(Instructor.class) // add classes so hibernate can be aware of them
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(InstructorDetail_BD.class)
                .addAnnotatedClass(Instructor_BD.class)
                .buildSessionFactory();

//        uni_DirectionalCreation(factory);
        bi_DirectionalCreation(factory);

    }

    private static void uni_DirectionalCreation(SessionFactory factory) {
        // create session

        Session session = factory.getCurrentSession();

        try {
            // create the objects
            Instructor instructor = new Instructor("Elon", "Musk", "elon@spacex.com");
            InstructorDetail instructorDetail = new InstructorDetail("https://spacex.com", "Challenges the world");

            // create associations between them
            instructor.setInstructorDetail(instructorDetail);

            // start a transaction
            session.beginTransaction();

            // save the instructor
            // NOTE: this will ALSO save the instructorDetail object because of the cascade type we have specified - CascadeType.ALL!!!
            System.out.println("Saving instructor: " + instructor);
            session.save(instructor);

            // commit transaction
            session.getTransaction().commit();
            System.out.println("Transaction committed successfully.");

        } finally {
            factory.close();
            System.out.println("Database connection closed.");
        }
    }

    private static void bi_DirectionalCreation(SessionFactory factory) {
        // create session

        Session session = factory.getCurrentSession();

        try {
            // create the objects
            Instructor_BD instructor = new Instructor_BD("Michael", "Jackson", "mj@shamoon.com");
            InstructorDetail_BD instructorDetail = new InstructorDetail_BD("https://mj.com", "Striking smooth criminals");

            // create associations between them
            instructor.setInstructorDetail(instructorDetail);
            instructorDetail.setInstructor(instructor);

            // start a transaction
            session.beginTransaction();

            // save the instructor
            // NOTE: this will ALSO save the instructorDetail object because of the cascade type we have specified - CascadeType.ALL!!!
            System.out.println("Saving instructor: " + instructor);
            session.save(instructor);

            // commit transaction
            session.getTransaction().commit();
            System.out.println("Transaction committed successfully.");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
            System.out.println("Database connection closed.");
        }
    }
}

