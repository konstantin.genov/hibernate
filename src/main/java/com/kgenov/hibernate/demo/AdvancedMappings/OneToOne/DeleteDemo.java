package com.kgenov.hibernate.demo.AdvancedMappings.OneToOne;
/* konstantin created on 3/7/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToOne */

import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.BiDirectional.InstructorDetail_BD;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.BiDirectional.Instructor_BD;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.Instructor;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DeleteDemo {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg-one-to-one-uni.xml") // specify correct hibernate file!
                .addAnnotatedClass(Instructor.class) // add classes so hibernate can be aware of them
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Instructor_BD.class)
                .addAnnotatedClass(InstructorDetail_BD.class)
                .buildSessionFactory();

//        uniDirectionalInstructorDelete(factory);
        biDirectionalInstructorDelete(factory);

    }

    private static void uniDirectionalInstructorDelete(SessionFactory factory) {
        int instructorId = 1;

        // start the transaction
        Session session = factory.getCurrentSession();
        session.beginTransaction();

        // fetch instructor by id
        Instructor instructorForDeletion = session.get(Instructor.class, instructorId);
        System.out.println("Found instructor: " + instructorForDeletion);

        // delete instructor
        if (instructorForDeletion != null) {
            System.out.println("Deleting: " + instructorForDeletion);

            // NOTE: will also delete the corresponding "details" object due to our cascade settings!!
            session.delete(instructorForDeletion);
        }

        // commit transaction
        session.getTransaction().commit();
        System.out.println("Transaction committed successfully.");
    }

    // Cascade deletion from OneToOne bi-directional relationship, we will delete the instructor detail, which in turn, will delete the corresponding instructor
    // This deletion works both ways, deleting the instructor deletes the detail and vice versa
    private static void biDirectionalInstructorDelete(SessionFactory factory) {
        Session session = factory.getCurrentSession();
        int detailId = 2;

        try {
            // start the transaction
            session.beginTransaction();

            // fetch InstructorDetail_BD by id
            Instructor_BD tempInstructorDetailBD = session.get(Instructor_BD.class, 2);
            System.out.println("Fetched bi-directional InstructorDetail " + tempInstructorDetailBD);
//            System.out.println("Corresponding bi-directional Instructor " + tempInstructorDetailBD.getInstructor());

            // delete instructor
            if (tempInstructorDetailBD != null) {
                System.out.println("Deleting Instructor detail: " + tempInstructorDetailBD);
//                        + " \n with corresponding Instructor: " + tempInstructorDetailBD.getInstructor());

                // NOTE: Due to the bi-directional relationship between the two classes, and the cascade type being specified as ALL, performing a delete on one of the objects
                // will delete the other one; deleting an instructor will delete the corresponding detail object and vice versa
                session.delete(tempInstructorDetailBD);
            }

            // commit transaction
            session.getTransaction().commit();
            System.out.println("Transaction committed successfully.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
            System.out.println("Database connection closed.");
        }

    }
}
