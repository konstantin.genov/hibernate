package com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.BiDirectional;
/* konstantin created on 3/7/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.UniDirectional.Entity */

import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.Instructor;

import javax.persistence.*;

// This InstructorDetail is bi-directional, contains local composition of Instructor class
@Entity
@Table(name = "instructor_detail")
public class InstructorDetail_BD {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    // The mappedBy property tells Hibernate to go into the Instructor class and look for the instructorDetail property
    // From then, it resolves the mapping by using the information from the @JoinColumn annotation, creating a bi-directional mapping/pointer
    // These are the annotations in the Instructor class:
    //    @OneToOne(cascade = CascadeType.ALL)
    //    @JoinColumn(name = "instructor_detail_id") -> Hibernate creates the relationship based on this annotation
    @OneToOne(mappedBy = "instructorDetail", cascade = CascadeType.ALL)
    private Instructor_BD instructor;

    @Column(name = "youtube_channel")
    private String youtubeChannel;

    @Column(name = "hobby")
    private String hobby;

    public InstructorDetail_BD() {
    }

    public InstructorDetail_BD(String youtubeChannel, String hobby) {
        this.youtubeChannel = youtubeChannel;
        this.hobby = hobby;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYoutubeChannel() {
        return youtubeChannel;
    }

    public void setYoutubeChannel(String youtubeChannel) {
        this.youtubeChannel = youtubeChannel;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public Instructor_BD getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor_BD instructor) {
        this.instructor = instructor;
    }

    @Override
    public String toString() {
        return "InstructorDetail{" +
                "id=" + id +
                ", youtubeChannel='" + youtubeChannel + '\'' +
                ", hobby='" + hobby + '\'' +
                '}';
    }
}
