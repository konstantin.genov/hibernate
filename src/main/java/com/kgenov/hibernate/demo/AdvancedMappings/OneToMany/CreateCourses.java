package com.kgenov.hibernate.demo.AdvancedMappings.OneToMany;
/* konstantin created on 3/13/2021 inside the package - com.kgenov.hibernate.demo.AdvancedMappings.OneToMany */

import com.kgenov.hibernate.demo.AdvancedMappings.OneToMany.Entities.Course;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToMany.Entities.CourseInstructor;
import com.kgenov.hibernate.demo.AdvancedMappings.OneToOne.Entities.UniDirectional.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateCourses {
    public static void main(String[] args) {
        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg-one-to-many.xml") // specify correct hibernate file!
                .addAnnotatedClass(CourseInstructor.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();



        Session session = factory.getCurrentSession();

        try {
            session.beginTransaction();

            int instructorId = 2;
            // fetch instructor by id
            CourseInstructor vicky = session.get(CourseInstructor.class, instructorId);

            // create courses
            Course drumCourse = new Course("The Art of Drums by Victoria Slavova");
            Course designCourse = new Course("Graphic Design - Ultimate Course by Victoria Slavova");

            // create relationship between the courses and the instructor
            vicky.addCourse(drumCourse);
            vicky.addCourse(designCourse);

            // save the courses
            session.save(drumCourse);
            session.save(designCourse);

            // commit changes
            session.getTransaction().commit();
            System.out.println("Transaction committed successfully.");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
            factory.close();
            System.out.println("Database connection closed.");
        }
    }
}
